# MSN kinetic monte carlo

We will be studying the diffusion of a hydrogen atom on a lattice using a simulation methode based on kinetic monte carlo. 

## Getting started

The first exercise is based on the diffusion of a hydrogen atom on a simple square lattice. 
It can diffuse in vertical or horizontal lines with a frequency gamma_1, and diagonally with a frequency gamma_2.

The second exercise is based on the diffusion of a hydrogen atom on a more complex lattice.
It can form a covalent bond as well as a hydrogen bond with two oxygens, and can either diffuse towards the oxygen in the hydrogen bond, or rotate by 90° to change the oxygen in the hydrogen bond. 