from array import array
from turtle import position
from venv import create
import numpy as np
import matplotlib.pyplot as plt

print(
    "Simulation of the diffusion of a hydrogen atom on a lattice (complexified model)"
)
print("###########################################################")
n_x = int(input("Enter the vertical size of the lattice : "))
n_y = int(input("Enter the horizontal size of the lattice : "))

grid_size = np.array((n_x, n_y))


gamma_1 = float(
    input(
        "Enter the jump frequency of change in covalent bond (between 0 and 1) : "
    ).replace(",", ".")
)

gamma_2 = 1 - gamma_1
gamma = gamma_1 + gamma_2

a = int(input("Please enter the lattice parameter in pm, usually around 300 : "))
b = int(
    input(
        "Please enter the length of the covalent bond OH in pm, usually around 100 pm : "
    )
)

n_steps = int(
    input(
        "Please enter the number of steps for the plotted simulation (At least several thousands): "
    )
)

# list of columns, each list is n_y long, and the global list is n_x long
# x is vertical, y is horizontal
# we add an other coordinate, (delta_x,delta_y), which corresponds to the position around the current oxygen atom with a covalent bond
# (the other oxygen makes a hydrogen bond)
# the position is ([x,y],[dx,dy])


def create_first_spot(grid_size):
    first_spot_x = np.random.randint(
        1, grid_size[0] - 1
    )  # we remove the borders to avoid considering impossible jumps in the other functions
    first_spot_y = np.random.randint(1, grid_size[1] - 1)

    choice_delta = np.random.randint(0, 2)
    delta = b / a * (2 * np.random.randint(0, 2) - 1)
    list_delta = [0, 0]
    list_delta[choice_delta] = delta
    return np.array((first_spot_x, first_spot_y)), np.array(list_delta)


# we print the grind using matplotlib
def print_grid(position_array):
    x = position_array[0][0] + position_array[1][0]
    y = position_array[0][1] + position_array[1][1]
    plt.xlim(0, grid_size[0])
    plt.ylim(0, grid_size[1])
    plt.plot(x, y, marker="o", markersize=10, markerfacecolor="green")
    plt.grid(markevery=1)
    plt.show()


def initialize(
    grid_size, first_spots
):  # first spots = tuple of array ([x_0,y_0],[delta_x0,delta_y0])
    first_spot_x = first_spots[0][0]
    first_spot_y = first_spots[0][1]
    first_delta_x = first_spots[1][0]
    first_delta_y = first_spots[1][1]

    grid = np.array(
        [[0 for i in range(grid_size[1])] for j in range(grid_size[0])], dtype=object
    )
    grid[first_spot_x][first_spot_y] = (first_delta_x, first_delta_y)
    return grid


#########################


def check_jump(x_gamma, gamma_1):  # check if the jump is done
    return 0 < x_gamma and x_gamma < gamma_1


def check_rot(x_gamma, gamma_1):  # check if a rotation is done
    return gamma_1 < x_gamma


# check if the position considered is inside the grid or not (we remove any coordinates thats go beyond the grid size)
# the difficulty lies in the delta around the border oxygens
def is_inside_grid(position, grid_size):
    x, y = position[0][0] + position[1][0], position[0][1] + position[1][1]
    return x >= 0 and y >= 0 and x <= grid_size[0] - 1 and y <= grid_size[1] - 1


# return array of list of possible new positions
def give_possible_jumps(position, grid_size):
    x = position[0][0]
    y = position[0][1]
    delta_x = position[1][0]
    delta_y = position[1][1]

    covalent_oxygen_pos = np.array(
        (round(x), round(y))
    )  # position of the oxygen in the covalent bond
    hydrogen_bond_oxygen_pos = np.array(
        (round(x) + (a / b) * delta_x, round(y) + (a / b) * delta_y)
    )  # position of the oxygen in the hydrogen bond

    new_possible_covalent_pos = (
        hydrogen_bond_oxygen_pos,
        np.array((-1 * delta_x, -1 * delta_y)),
    )  # the possible jump is towards the oxygen in the previous hydrogen bond
    new_possible_hydr_pos1 = (
        covalent_oxygen_pos,
        np.array((delta_y, delta_x)),
    )  # the possible rotations have the same covalent oygen
    new_possible_hydr_pos2 = (
        covalent_oxygen_pos,
        np.array((-1 * delta_y, -1 * delta_x)),
    )  # these rotations are on the perpendicular axis to the current hydrogen bond

    list_possible_rotations = np.array((new_possible_hydr_pos1, new_possible_hydr_pos2))
    list_rotations = []
    for index, rot in enumerate(
        list_possible_rotations
    ):  # checking all possible rotations
        if is_inside_grid(rot, grid_size):  # if one of the position is inside
            list_rotations.append(rot)
    return (new_possible_covalent_pos, np.array(list_rotations))
    # tuple : ( ([x_jump,y_jump],[dx_jump,dy_jump]) , [ ([x_rot1,y_rot1],[dx_rot1,dy_rot1]) , ([x_rot2,y_rot2],[dx_rot2,dy_rot2]) ] )


def choose_rotation(list_rot):
    return list_rot[np.random.randint(len(list_rot))]


# simulating one single step
def obtain_new_position(position, gamma_1, gamma, grid_size):
    x_1 = np.random.random()
    x_gamma = x_1 * gamma
    is_jumping = check_jump(x_gamma, gamma_1)
    is_rotating = check_rot(x_gamma, gamma_1)
    list_new_positions = give_possible_jumps(position, grid_size)
    new_position = position
    # tuple : ( ([x_jump,y_jump],[dx_jump,dy_jump]) , [ ([x_rot1,y_rot1],[dx_rot1,dy_rot1]) , ([x_rot2,y_rot2],[dx_rot2,dy_rot2]) ] )
    if is_jumping:
        new_position = list_new_positions[0]  # a tuple of arrays
    elif is_rotating:
        list_rotations = list_new_positions[1]  # an array of tuples of arrays
        new_position = choose_rotation(list_rotations)
    return new_position


def save_trajectory(file, list_trajectory):
    f = open(file, "a")
    for x in list_trajectory:
        f.write(str(x[0]) + "," + str(x[1]) + "\n")
    f.close()


def do_simulation(n_steps, gamma_1, gamma, grid_size, file="none"):
    first_spots = create_first_spot(grid_size)
    list_positions = [[first_spots[0].tolist(), first_spots[1].tolist()]]
    grid = initialize(grid_size, first_spots)

    for i in range(n_steps):
        current_position = np.array(list_positions[-1])
        new_position = obtain_new_position(current_position, gamma_1, gamma, grid_size)
        list_positions.append([new_position[0].tolist(), new_position[1].tolist()])
    if file != "none":
        save_trajectory(file, list_positions)
    return np.array(list_positions)


list_positions = do_simulation(n_steps, gamma_1, gamma, grid_size)

# function to go from the previous list definition to the one used in exercice 1
def calculate_usual_position_list(list_positions):
    new_list_positions = []
    for position in list_positions:
        position_element = [
            position[0][0] + position[1][0],
            position[0][1] + position[1][1],
        ]
        new_list_positions.append(position_element)
    return np.array(new_list_positions)


usual_list_positions = calculate_usual_position_list(list_positions)


# function to calculate the diffusion coefficient for a fixed time window of the simulation
# n_0 is the first step to consider, and delta_n is the number of steps to consider
def calculate_D_fixed_window(n_0, list_pos, delta_n):
    # we only keep the positions of the time window of interest
    sub_list_to_consider = [i for i in list_pos[n_0 : n_0 + delta_n + 1]]
    # we calculate the difference in positions between each position and the initial position
    array_D_temp = [r - sub_list_to_consider[0] for r in sub_list_to_consider[1:]]
    # we calculate 2*dimension*D*delta_n*dt using its definition
    array_D = [D[0] ** 2 + D[1] ** 2 for D in array_D_temp]
    result = np.mean(array_D, axis=0)
    return result


# we calculate D using a sliding origin time method
def calculate_D(list_pos, delta_n=100):
    numb_pos = len(list_pos)
    list_sliding_D = []
    n_0 = 0
    while n_0 + delta_n < numb_pos:
        current_D = calculate_D_fixed_window(n_0, list_pos, delta_n)
        list_sliding_D.append(current_D.tolist())
        n_0 += 1
    return (1 / (4 * delta_n)) * np.mean(list_sliding_D, axis=0)


print(
    "The diffusion coefficient for this simulation is : ",
    calculate_D(usual_list_positions),
    "\n",
)

#####################################################################################################################
print("This part of the simulation is dedicated to drawing key_parameters. \n")


def draw_trajectory(list_positions):
    X = list_positions[:, 0]
    Y = list_positions[:, 1]
    plt.title("Movement of the Hydrogen atom on the square lattice")
    plt.xlabel("axis X")
    plt.ylabel("axis Y")
    plt.plot(X, Y)
    plt.axis("equal")
    plt.show()


def draw_trajectory_anim(n_steps, list_positions, n_steps_plot=100):
    # Animation
    X = list_positions[:, 0]
    Y = list_positions[:, 1]

    x = []
    y = []
    index = 0
    while index < n_steps:
        for j in range(n_steps_plot):
            if index < n_steps:
                x.append(X[index])
                y.append(Y[index])
            index += 1
        plt.plot(x, y, "b")
        plt.axis("equal")
        plt.pause(0.15)
    plt.show()


draw_trajectory(usual_list_positions)
draw_trajectory_anim(n_steps, usual_list_positions, 1000)

print("Evolution of the diffusion coefficient as a function of the jump frequency. \n")
n_steps = int(input("Enter the number of steps for the simulation : "))
n_points = int(input("Enter the number of points for the curve : "))
n_simuls = int(input("Enter the number of simulations for each point : "))


# function to draw the evolution of the diffusion coefficients according to the parameters
# it averages over several simulations
# n_steps is the number of steps for each simulation
# n_points is the number of points on the curve
# n_simul is the number of simulations to calculate the average of each point
def draw_diffusion_coeff(n_steps, n_points, gamma, n_simuls):
    d_gamma = gamma / (n_points - 1)
    D_simu = []
    Frequency_list = [k * d_gamma for k in range(n_points)]
    for k in range(n_points):
        d_simul = 0
        for j in range(n_simuls):
            print("Running simulation for the diffusion coefficient...")
            list_pos = do_simulation(
                n_steps, k * d_gamma, gamma, grid_size, file="none"
            )
            d_moy = np.mean(calculate_D(list_pos))
            d_simul += d_moy
        D_simu.append(d_simul / n_simuls)
    plt.title("Coefficient of Hydrogen diffusion on a square lattice")
    plt.xlabel("Relative frequency Gamma1/Gamma")
    plt.ylabel("Normalized diffusion coefficient")
    plt.scatter(Frequency_list, D_simu)
    plt.legend()
    plt.show()


draw_diffusion_coeff(n_steps, n_points, gamma, n_simuls)
