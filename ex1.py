from re import X
from time import time
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

print("Simulation of the diffusion of a hydrogen atom on a lattice")
print("###########################################################")
n_x = int(input("Enter the vertical size of the lattice : "))
n_y = int(input("Enter the horizontal size of the lattice : "))

grid_size = np.array((n_x, n_y))
# simulation_time=1

gamma_1 = float(
    input(
        "Enter the jump frequency of the horizontal and vertical directions (between 0 and 1) : "
    ).replace(",", ".")
)

gamma = 1
gamma_2 = gamma - gamma_1
file = "./positions.txt"
dt = 1 / gamma

n_steps = int(
    input(
        "Please enter the number of steps for the plotted simulation (At least several thousands): "
    )
)

# we consider a grid to visualize our problem
# 1 where the hydrogen atom is, 0 otherwise
# list of columns, each list is n_y long, and the global list is n_x long
# x is vertical, y is horizontal

# function to create the first spot of the simulation
def create_first_spot(grid_size):
    first_spot_x = int(grid_size[0] / 2)
    first_spot_y = int(grid_size[1] / 2)
    return np.array((first_spot_x, first_spot_y))


# function to initialize the grid
def initialize(grid_size):
    first_spots = create_first_spot(grid_size)
    first_spot_x = first_spots[0]
    first_spot_y = first_spots[1]

    grid = np.array([[0 for i in range(grid_size[1])] for j in range(grid_size[0])])
    grid[first_spot_x][first_spot_y] = 1
    return grid


# checking if the jump is done either vertically or horizontally
def check_vert_horiz_jump(x_gamma, gamma_1):
    return 0 < x_gamma and x_gamma < gamma_1


# checking if the jump is done diagonally
def check_diagonal_jump(x_gamma, gamma_1):
    return gamma_1 < x_gamma


# pair of integers for each direction, x then y
# (0,1)=right, (-1,0)=up, (0,-1)=left, (1,0)=down
# (-1,1)=up-right, (-1,-1)=up-left, (1,-1)=down-left, (1,1)=down-right
right = np.array((0, 1))
left = np.array((0, -1))
down = np.array((1, 0))
up = np.array((-1, 0))
down_right = np.array((1, 1))
down_left = np.array((1, -1))
up_right = np.array((-1, 1))
up_left = np.array((-1, -1))

# function to return the array of possible directions
def give_possible_directions(position, grid_size):
    x = position[0]
    y = position[1]
    # checking if the hydrogen is on any border
    is_left = y == 0
    is_right = y == grid_size[1] - 1
    is_top = x == 0
    is_bottom = x == grid_size[0] - 1

    possible_directions_10 = [right, left, down, up]
    possible_directions_11 = [down_left, down_right, up_left, up_right]
    # for each border we give the possible directions
    if is_left:
        if is_top:
            possible_directions_10 = [down, right]
            possible_directions_11 = [down_right]
        elif is_bottom:
            possible_directions_10 = [up, right]
            possible_directions_11 = [up_right]
        else:
            possible_directions_10 = [right, up, down]
            possible_directions_11 = [up_right, down_right]
    elif is_right:
        if is_top:
            possible_directions_10 = [left, down]
            possible_directions_11 = [down_left]
        elif is_bottom:
            possible_directions_10 = [up, left]
            possible_directions_11 = [up_left]
        else:
            possible_directions_10 = [up, down, left]
            possible_directions_11 = [up_left, down_left]

    elif is_top:
        possible_directions_10 = [down, right, left]
        possible_directions_11 = [down_right, down_left]

    elif is_bottom:
        possible_directions_10 = [up, left, right]
        possible_directions_11 = [up_left, up_right]

    return np.array((possible_directions_10, possible_directions_11))


# randomly choose a direction between the possible directions
def choose_direction(list):
    return list[np.random.randint(len(list))]


# obtain the new position by calling the function for the possible directions and the choice
def obtain_new_position(position, gamma_1, gamma, grid_size):
    x_1 = np.random.random()
    x_gamma = x_1 * gamma
    bool_direction_hor_vert = check_vert_horiz_jump(
        x_gamma, gamma_1
    )  # check if the vertical or horizontal jump is possible
    bool_direction_diag = check_diagonal_jump(
        x_gamma, gamma_1
    )  # check if the diagonal jump is possible
    new_position = position
    possible_directions_array = give_possible_directions(position, grid_size)
    if bool_direction_hor_vert:
        direction_array = choose_direction(possible_directions_array[0])
        new_position = position + direction_array

    elif bool_direction_diag:
        direction_array = choose_direction(possible_directions_array[1])
        new_position = position + direction_array

    return new_position


# function to update the grid
def update_grid(grid, old_position, new_position):
    old_x = old_position[0]
    old_y = old_position[1]
    new_x = new_position[0]
    new_y = new_position[1]
    grid[old_x][old_y] = 0
    grid[new_x][new_y] = 1
    return grid


# function to save the trajectory (list of positions is a txt file)
def save_trajectory(file, list_trajectory):
    f = open(file, "a")
    for x in list_trajectory:
        f.write(str(x[0]) + "," + str(x[1]) + "\n")
    f.close()


# function to run the entire simulation
def do_simulation(n_steps, gamma_1, gamma, grid_size, file="none"):
    first_spots = create_first_spot(grid_size)
    list_positions = [first_spots.tolist()]
    grid = initialize(grid_size)
    # for each step of the simulation
    for i in range(n_steps):
        current_position = np.array(list_positions[-1])
        new_position = obtain_new_position(current_position, gamma_1, gamma, grid_size)
        list_positions.append(new_position.tolist())
        update_grid(grid, current_position, new_position)
    if file != "none":  # if a file is specified, the trajectory is saved
        save_trajectory(file, list_positions)
    return np.array(list_positions)


# list_positions is a list of [x,y]

# function to calculate the diffusion coefficient for a fixed time window of the simulation
# n_0 is the first step to consider, and delta_n is the number of steps to consider
def calculate_D_fixed_window(n_0, list_pos, delta_n):
    # we only keep the positions of the time window of interest
    sub_list_to_consider = [i for i in list_pos[n_0 : n_0 + delta_n + 1]]
    # we calculate the difference in positions between each position and the initial position
    array_D_temp = [r - sub_list_to_consider[0] for r in sub_list_to_consider[1:]]
    # we calculate 2*dimension*D*delta_n*dt using its definition
    array_D = [D[0] ** 2 + D[1] ** 2 for D in array_D_temp]
    result = np.mean(array_D, axis=0)
    return result


# we calculate D using a sliding origin time method
def calculate_D(list_pos, delta_n=100):
    numb_pos = len(list_pos)
    list_sliding_D = []
    n_0 = 0
    while n_0 + delta_n < numb_pos:
        current_D = calculate_D_fixed_window(n_0, list_pos, delta_n)
        list_sliding_D.append(current_D.tolist())
        n_0 += 1
    return (1 / (4 * delta_n)) * np.mean(list_sliding_D, axis=0)


list_positions = do_simulation(n_steps, gamma_1, gamma, grid_size)

print(
    "The diffusion coefficient for this simulation is : ",
    calculate_D(list_positions),
    "\n",
)

#####################################################################################################################
print("This part of the simulation is dedicated to drawing key_parameters. \n")


def draw_trajectory(list_positions):
    X = list_positions[:, 0]
    Y = list_positions[:, 1]
    plt.title("Movement of the Hydrogen atom on the square lattice")
    plt.xlabel("axis X")
    plt.ylabel("axis Y")
    plt.plot(X, Y)
    plt.axis("equal")
    plt.show()


def draw_trajectory_anim(n_steps, list_positions, n_steps_plot=100):
    # Animation
    X = list_positions[:, 0]
    Y = list_positions[:, 1]

    x = []
    y = []
    index = 0
    while index < n_steps:
        for j in range(n_steps_plot):
            if index < n_steps:
                x.append(X[index])
                y.append(Y[index])
            index += 1
        plt.plot(x, y, "b")
        plt.axis("equal")
        plt.pause(0.15)
    plt.show()


draw_trajectory(list_positions)
draw_trajectory_anim(n_steps, list_positions, 1000)

print("Evolution of the diffusion coefficient as a function of the jump frequency. \n")
n_steps = int(input("Enter the number of steps for the simulation : "))
n_points = int(input("Enter the number of points for the curve : "))
n_simuls = int(input("Enter the number of simulations for each point : "))


# function to draw the evolution of the diffusion coefficients according to the parameters
# it averages over several simulations
# n_steps is the number of steps for each simulation
# n_points is the number of points on the curve
# n_simul is the number of simulations to calculate the average of each point
def draw_diffusion_coeff(n_steps, n_points, gamma, n_simuls):
    d_gamma = gamma / (n_points - 1)
    D_simu = []
    Frequency_list = [k * d_gamma for k in range(n_points)]
    D_th = [1 / 4 * (x + (1 - x) * 2) for x in Frequency_list]
    for k in range(n_points):
        d_simul = 0
        for j in range(n_simuls):
            print("Running simulation for the diffusion coefficient...")
            list_pos = do_simulation(
                n_steps, k * d_gamma, gamma, grid_size, file="none"
            )
            d_moy = np.mean(calculate_D(list_pos))
            d_simul += d_moy
        D_simu.append(d_simul / n_simuls)
    plt.title("Coefficient of Hydrogen diffusion on a square lattice")
    plt.xlabel("Relative frequency Gamma1/Gamma")
    plt.ylabel("Normalized diffusion coefficient")
    plt.scatter(Frequency_list, D_simu)
    plt.plot(Frequency_list, D_th, "-r", label="Theory")
    plt.legend()
    plt.show()


draw_diffusion_coeff(n_steps, n_points, gamma, n_simuls)
